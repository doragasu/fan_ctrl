# Introduction

This is a simple light control program with Telegram connectivity, OTA upgrades support and AP provisioning.

As the purpose of this project is very specific, it is not intended to be used as-is by other developers, but can be used as an example about how to implement the included functions. Code has been built using [ESP8266\_RTOS\_SDK](https://github.com/espressif/ESP8266_RTOS_SDK) version 3.3. The project can also be built with version 3.4, but it seems WiFi connection is a bit unstable with this version (your mileage may vary).

# Provisioning

Provisioning can be done with Espressif SoftAP provisioning app version 2.0.13. You can download the Android APK [here](https://github.com/espressif/esp-idf-provisioning-android/releases/download/lib-2.0.13/ESP_SoftAP_Prov_2_0_13.apk). Install it and then generate a QR code with the information matching the one in the menuconfig. For example:

```json
{"ver":"v1","name":"FAN_PROVISIONING","password":"Pr0v1sIOn","transport":"softap"}
```

Launch the app, scan the QR and enter the WiFi credentials when asked.

# Author

This project has been built and coded by Jesús Alonso (doragasu).

# License

The code in this repository comes with NO WARRANTY and is provided under the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.en.html).
