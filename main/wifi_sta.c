#include <string.h>
#include <esp_event_loop.h>
#include <esp_wifi.h>

#include "sdkconfig.h"
#include "util.h"
#include "app_prov.h"
#include "wifi_sta.h"

static wifi_event_cb event_cb = NULL;
static bool provision_active = false;
static uint32_t connection_attempts = 0;

static void provision_start(void)
{
	provision_active = true;
	connection_attempts = 0;
	LOGI("starting provisioning");
	app_prov_start_softap_provisioning(CONFIG_WIFI_PROV_SSID,
			CONFIG_WIFI_PROV_PASSWORD, true, NULL);
}

static void disconnected_proc(system_event_sta_disconnected_t *dis)
{
	union wifi_event_data data;
	enum wifi_event event = WIFI_EVENT_DISASSOC;

	connection_attempts++;
	LOGW("attempt: %d, reason: %d", connection_attempts, dis->reason);

	if (connection_attempts < CONFIG_WIFI_CONN_ATTEMPTS) {
		if (dis->reason == WIFI_REASON_BASIC_RATE_NOT_SUPPORT) {
			// switch to 802.11 bgn mode
			esp_wifi_set_protocol(ESP_IF_WIFI_STA, WIFI_PROTOCOL_11B |
					WIFI_PROTOCOL_11G | WIFI_PROTOCOL_11N);
		}
		esp_wifi_connect();
	} else {
		// Desist association and start provisioning mode
		esp_wifi_stop();
		provision_start();
		event = WIFI_PROVISION_START;
	}

	if (event_cb) {
		memcpy(data.disassoc.bssid, dis->bssid, 6);
		event_cb(event, &data);
	}
}

static void connected_proc(system_event_sta_connected_t *con)
{
	union wifi_event_data data;

	LOGI("BSSID: " MACSTR, MAC2STR(con->bssid));
	if (event_cb) {
		memcpy(data.assoc.bssid, con->bssid, 6);
		event_cb(WIFI_EVENT_ASSOC, &data);
	}
}

static void got_ip_proc(system_event_sta_got_ip_t *got_ip)
{
	union wifi_event_data data;

	LOGI("IPv4: %s, gw: %s", ip_ntoa(&got_ip->ip_info.ip),
			ip_ntoa(&got_ip->ip_info.gw));

	connection_attempts = 0;
	if (event_cb) {
		data.got_ip.ip_info = got_ip->ip_info;
		event_cb(WIFI_EVENT_GOT_IP, &data);
	}
}

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
	UNUSED_PARAM(ctx);
	system_event_info_t *info = &event->event_info;

	// When provisioning is active, forward events to its event handler
	if (provision_active) {
		app_prov_event_handler(ctx, event);
	}

	switch(event->event_id) {
	case SYSTEM_EVENT_STA_START:
		esp_wifi_connect();
		break;

	case SYSTEM_EVENT_STA_CONNECTED:
		connected_proc(&info->connected);
		break;

	case SYSTEM_EVENT_STA_GOT_IP:
		got_ip_proc(&info->got_ip);
		break;

	case SYSTEM_EVENT_STA_DISCONNECTED:
		disconnected_proc(&info->disconnected);
		break;

	default:
		break;
	}

	return ESP_OK;
}

void wifi_init_sta(wifi_event_cb _event_cb)
{
	bool provisioned = false;

	connection_attempts = 0;
	provision_active = false;
	event_cb = _event_cb;

	tcpip_adapter_init();
	ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL) );

	// If device is provisioned, tries to associate.
	// Else start provisioning process.
	ESP_ERROR_CHECK(app_prov_is_provisioned(&provisioned));
	if (provisioned) {
		ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
		ESP_ERROR_CHECK(esp_wifi_start());
	} else {
		provision_start();
	}
}

