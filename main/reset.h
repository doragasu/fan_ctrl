#ifndef __BOOT_H__
#define __BOOT_H__

#include <esp_system.h>
#include <stdint.h>

esp_reset_reason_t rst_reason_get(void);
const char *rst_reason_str_get(esp_reset_reason_t reason);

__attribute__((noreturn)) void rst_sw_rst(void);
void rst_deepsleep(void);

void rst_rtc_mem_write(const uint32_t *data, uint32_t dw_offset, uint32_t dw_len);
void rst_rtc_mem_read(uint32_t *data, uint32_t dw_offset, uint32_t dw_len);

#endif
