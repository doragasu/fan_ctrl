#ifndef _LIGHT_H_
#define _LIGHT_H_

enum light_stat {
	LIGHT_OFF = 0,
	LIGHT_ON = 1
};

void light_start(int in_pin, int out_pin, enum light_stat initial_state);
void light_toggle(void);
enum light_stat light_get(void);

#endif /*_LIGHT_H_*/
