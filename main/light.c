#include <driver/gpio.h>
#include <esp_timer.h>
#include <esp_attr.h>

#include "util.h"
#include "light.h"

#define PIN_MASK(pin_number) (1UL<<(pin_number))

static enum light_stat light = LIGHT_ON;
static int out_pin;

// 500 ms debounce time
#define DEBOUNCE_US	500000

void IRAM_ATTR light_toggle(void)
{
	static uint64_t last = 0;
	uint64_t current = esp_timer_get_time();

	if ((current - last) > DEBOUNCE_US) {
		light ^= 1;
		gpio_set_level(out_pin, light);
		last = current;
	}
}

static IRAM_ATTR void light_toggle_cb(void *arg)
{
	UNUSED_PARAM(arg);

	light_toggle();
}

void light_start(int in_pin, int _out_pin, enum light_stat initial_state)
{
	const gpio_config_t in_cfg = {
		.mode = GPIO_MODE_INPUT,
		.pull_up_en = GPIO_PULLUP_ENABLE,
		.pin_bit_mask = PIN_MASK(in_pin),
		.intr_type = GPIO_INTR_ANYEDGE
	};
	const gpio_config_t out_cfg = {
		.mode = GPIO_MODE_OUTPUT,
		.pin_bit_mask = PIN_MASK(_out_pin)
	};

	// Configure output and set initial light state
	gpio_config(&out_cfg);
	light = initial_state;
	gpio_set_level(_out_pin, light);
	out_pin = _out_pin;

	// Configure input pin
	gpio_config(&in_cfg);
	//install gpio isr service
	gpio_install_isr_service(0);
	//hook isr handler for specific gpio pin
	gpio_isr_handler_add(in_pin, light_toggle_cb, (void*)in_pin);
}

enum light_stat light_get(void)
{
	return light;
}
