#include <nvs_flash.h>
#include <string.h>

#include "esp_system.h"
#include "wifi_sta.h"
#include "light.h"
#include "reset.h"
#include "telegram.h"
#include "util.h"

// WARNING: D0 must be externally wired to RST for the deep sleep reset to work.

// Light toggle sense pin
#define LIGHT_TOGGLE_PIN 4 // D2 on NodeMCU
// Relay control output pin
#define LIGHT_OUT_PIN 5    // D1 on NodeMCU

static bool tg_started = false;
static esp_reset_reason_t rst_reason;

void wifi_ev_cb(enum wifi_event event, union wifi_event_data *data)
{
	UNUSED_PARAM(data);
	char buf[64];

	if (WIFI_EVENT_GOT_IP == event && !tg_started) {
		char *msg = NULL;
		if (ESP_RST_POWERON != rst_reason && ESP_RST_EXT != rst_reason) {
			snprintf(buf, sizeof(buf), "WARNING: RESET DUE TO: %s",
					rst_reason_str_get(rst_reason));
			msg = buf;
		}
		tg_started = !tg_parser_start(msg, NULL);
	}
}

void app_main(void)
{
	enum light_stat light = LIGHT_ON;

	rst_reason = rst_reason_get();
	// On power-on reset, we keep the light ON by default. Else try reading
	// the value from the RTC memory. As the RTC memory only preserves its
	// value when exiting from deep sleep, on all other events (software
	// reset, watchdog reset, etc.) the ligth will be turned off.
	if (ESP_RST_POWERON == rst_reason || ESP_RST_EXT == rst_reason) {
		LOGI("ligth ON after POWER or EXT reset");
	} else if (ESP_RST_DEEPSLEEP == rst_reason) {
		rst_rtc_mem_read(&light, 0, 1);
		LOGI("restoring light status: %d", light);
	} else {
		light = LIGHT_OFF;
		LOGI("unexpected reset reason, setting light OFF");
	}

	// Configure light module early, for the light to be turned on
	// as soon as possible when device is powered
	light_start(LIGHT_TOGGLE_PIN, LIGHT_OUT_PIN, light);
	LOGI("STARTING FAN CONTROL VERSION " FLCTRL_VERSION);

	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);

	wifi_init_sta(wifi_ev_cb);
	ESP_ERROR_CHECK(tg_init());
}
