#include <esp_https_ota.h>
#include <string.h>
#include "util.h"
#include "upgrade.h"


/// Endpoint for update API
#define UPDATE_EP "/fan_ctrl/"

#define URL_MAX_LEN 255

extern const uint8_t cert[] asm("_binary_ca_cert_pem_start");

static size_t append(char * dst, size_t pos, const char * org)
{
	size_t to_copy = MIN(strlen(org), URL_MAX_LEN - pos);

	memcpy(dst + pos, org, to_copy + 1);

	return pos + to_copy;
}

esp_err_t upgrade_firmware(const char *server, const char *name)
{
	char url[URL_MAX_LEN + 1] = "http://";
	int pos = 7;
	esp_http_client_config_t cfg = {};
	esp_err_t err;

	pos = append(url, pos, server);
	pos = append(url, pos, UPDATE_EP);
	pos = append(url, pos, name);

	LOGI("upgrading from %s", url);
	cfg.url = url;
//	cfg.cert_pem = cert;
	cfg.cert_pem = NULL;

	err = esp_https_ota(&cfg);

	if (err) {
		LOGE("upgrade failed with code %d", err);
	} else {
		LOGI("upgrade succeeded");
	}

	return err;
}

