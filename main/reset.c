#include <esp_sleep.h>
#include <esp_attr.h>
#include <string.h>

#include "util.h"
#include "reset.h"

#define RTC_DATA_LEN 512
#define RTC_DATA_ARRAY_LEN ((RTC_DATA_LEN) / sizeof(uint32_t))

// RTC memory (that can survive deep sleep). Note that RTC memory accesses
// must be 32-bit aligned
static RTC_DATA_ATTR uint32_t rtc_data[RTC_DATA_ARRAY_LEN] = {};

// Reset reason strings
static const char * const rst_reason_str[] = {
	"UNKNOWN",   // Reset reason can not be determined
	"POWERON",   // Reset due to power-on event
	"EXT",       // Reset by external pin (not applicable for ESP8266)
	"SW",        // Software reset via esp_restart
	"PANIC",     // Software reset due to exception/panic
	"INT_WDT",   // Reset (software or hardware) due to interrupt watchdog
	"TASK_WDT",  // Reset due to task watchdog
	"WDT",       // Reset due to other watchdogs
	"DEEPSLEEP", // Reset after exiting deep sleep mode
	"BROWNOUT",  // Brownout reset (software or hardware)
	"SDIO",      // Reset over SDIO
	"FAST_SW"    // Fast reboot
};

esp_reset_reason_t rst_reason_get(void)
{
	return esp_reset_reason();
}

const char *rst_reason_str_get(esp_reset_reason_t reason)
{
	if (reason < 0 || reason >= ARRAY_SIZE(rst_reason_str)) {
		reason = 0;
	}

	return rst_reason_str[reason];
}

__attribute__((noreturn)) void rst_sw_rst(void)
{
	esp_restart();
}

void rst_deepsleep(void)
{
	esp_deep_sleep(1);
}

void rst_rtc_mem_write(const uint32_t *data, uint32_t dw_offset, uint32_t dw_len)
{
	for (uint32_t i = 0; i < dw_len; i++) {
		rtc_data[dw_offset + i] = data[i];
	}
}

void rst_rtc_mem_read(uint32_t *data, uint32_t dw_offset, uint32_t dw_len)
{
	for (uint32_t i = 0; i < dw_len; i++) {
		data[i] = rtc_data[dw_offset + i];
	}
}
