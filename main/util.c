#include <stdlib.h>
#include <string.h>

#include "util.h"

int hex_nibble_to_bin(char nibble)
{
	int result;

	if (nibble >= '0' && nibble <= '9') {
		result = nibble - '0';
	} else if (nibble >= 'a' && nibble <= 'f') {
		result = 0xa + nibble - 'a';
	} else if (nibble >= 'A' && nibble <= 'F') {
		result = 0xA + nibble - 'A';
	} else {
		result = -1;
	}

	return result;
}

int hex_byte_to_bin(const char *hex_byte)
{
	int result = 0;
	int nibble;

	for (int i = 0; i < 2; i++) {
		result <<= 4;
		nibble = hex_nibble_to_bin(hex_byte[i]);
		if (nibble < 0) {
			return -1;
		}
		result |= nibble;
	}

	return result;
}

int mac_to_bin(const char *mac, uint8_t bin[6])
{
	int byte;
	int i;

	for (i = 0; i < 6 && *mac; i++) {
		byte = hex_byte_to_bin(mac);
		if (byte < 0) {
			return -1;
		}
		bin[i] = byte;
		mac += 2;
		if (*mac == ':' || *mac == '-') {
			mac++;
		}
	}
	if (i < 6) {
		return -1;
	}

	return 0;
}

int token_index(const char **tokens, const char *search_str)
{
	int result = -1;

	for (int i = 0; result == -1 && tokens[i]; i++) {
		if (0 == strcmp(search_str, tokens[i])) {
			result = i;
		}
	}

	return result;
}

void *memdup(const void *mem, size_t len)
{
	void *dup = NULL;
	
	if (mem && len) {
		dup = malloc(len);
	}

	if (dup) {
		memcpy(dup, mem, len);
	}

	return dup;
}

